<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Otf;

class HomeController extends Controller
{
	public function index()
	{
		$productionList = $this->getProductionSites();
		$preProductionList = $this->getPreProductionSites();

		return view('home', compact('productionList','preProductionList'));
	}

	public function getNewInstanceForm()
	{
		return view('newInstance');
	}

	public function setNewInstance(Request $request)
	{
		$this->validate($request, [
        	'instance' => 'required|max:50',
        	'domain' => 'required|max:100',
        	'customerName' => 'required|max:255',
        	'telephone' => 'required|numeric',
        	'address' => 'required|max:255'
    	]);

		if($this->querySqlServer("select * from appSettings WHERE  SubDomain = '".$request->input("instance")."'"))
		{
			return back()->withErrors(['instance' => 'The Instance must not already exist.']);
		} 

		$result = "";

		//Create Config files
		$result = $this->createConfigFiles($request);

		return $result;
	}

	private function createConfigFiles($request)
	{
		$psConnectionString = " -connectionString '".$this->getConnectionString() ."'";
		$psSiteID = " -siteId 'test10'";
		$psCreateInstance = " -createInstance 'instance;domain;customerName;telephone;address'";
		$psInstance = " -instance '".$request->input('instance'). "'";
		$psPath = 'C:\\Windows\\System32\WindowsPowerShell\v1.0\\powershell.exe';
		$psDIR = resource_path('assets\\ps1\\');;
		$psScript = "createConfigFiles.ps1";
		$psParam = $psConnectionString. $psSiteID. $psInstance. $psCreateInstance;
		$runScript = $psDIR. $psScript. $psParam;
		$runCMD = $psPath. ' -ExecutionPolicy RemoteSigned '. $runScript;
		exec($runCMD, $out);
		return $out;
	}

    public function getProductionSites()
    {

    	$listSubDomains = $this->getSubDomains(true);

		return $listSubDomains;
    }

    public function getPreProductionSites()
    {

    	$listSubDomains = $this->getSubDomains();

		return $listSubDomains;
    }

    public function getSubDomains($production = false)
    {
    	if($production)
    	{
    		$appSettings = $this->querySqlServer("select SubDomain FROM AppSettings WHERE inProduction = 'true'", true);
    	}else{
    		$appSettings = $this->querySqlServer("select SubDomain FROM AppSettings WHERE inProduction != 'true'");
    	}

    	$listSubDomains = collect();
		
		foreach($appSettings as  $appSetting)
		{
			$listSubDomains->push($appSetting->SubDomain);
		}

    	return $listSubDomains;
    }

    private function querySqlServer($query, $production = false)
	{    	
    	if($production)
    	{
	    	$options = array(
				'driver' => 'sqlsrv',
				'database' => "EnjoviaManager",
			);
		}else{
			$options = array(
				'driver' => 'sqlsrv',
				'host' => "enjovia-test.c5fopf8mwxer.eu-west-1.rds.amazonaws.com",
				'username' => 'Enjovia',
				'password' => 'J0hns0n44!?!',
				'database' => "EnjoviaManager-test",
			);
		}

		$otf = new Otf($options);
		$results = $otf->getConnection()->select($query);

		return $results;
    }

    private function getConnectionString($production = false)
	{
		if($production)
		{
			$connectionString = "";
		}else{
			$connectionString = "Server=tcp:enjovia-test.c5fopf8mwxer.eu-west-1.rds.amazonaws.com,1433;User ID=Enjovia;Password=J0hns0n44!?!;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;TrustServerCertificate=True;";
		}
		return $connectionString;
	}

	private function createDatabase()
	{

	}
}
