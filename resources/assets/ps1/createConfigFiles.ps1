#*--------------------------------------------------------------------------------------------------------------------------------------------------------
#* Function: ConfigFileCreation
#* Author: Sami Souabni (sami@enjovia.com)
#*--------------------------------------------------------------------------------------------------------------------------------------------------------

	param (
			[string] $connectionString = $(throw "Please specify a server connection string."),
			[string] $siteId = $(throw "Please specify an analytics site ID."),
			[string] $instance = $(throw "Please specify an instance name."),
			[string] $createInstance = $(throw "Please specify a createInstance string.")
			)

	$configFiles0=""
	$configFiles1=""
	$cusName = $createInstance.Split(';')[2]

	if (Test-Path -Path "C:\Users\Administrator\Desktop\test\Web.appSettings.$instance.config")
	{
		Write-Output "C:\Users\Administrator\Desktop\test\Web.appSettings.$instance.config exists!"
	}
	else
	{
		# Create a new XML File with config root node
		[System.XML.XMLDocument]$oXMLDocument=New-Object System.XML.XMLDocument
		# New Node
		[System.XML.XMLElement]$oXMLRoot=$oXMLDocument.CreateElement("appSettings")
		# Append as child to an existing node
		$oXMLDocument.appendChild($oXMLRoot)

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key","MailGun.PublicApi")
		$oXMLAdd.SetAttribute("value","pubkey-05xadu70xe-daicmuy4tmi360oto7it5")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key","NewRelic.AppName")
		$oXMLAdd.SetAttribute("value","VoucherShop - $cusName")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key","NewRelic.Enabled")
		$oXMLAdd.SetAttribute("value","true")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key","StorageConnectionString")
		$oXMLAdd.SetAttribute("value","DefaultEndpointsProtocol=https;AccountName=vouchapp;AccountKey=krA5cMeeMw4B5S899po0DatN1bQYMhw/OfsV71B5hI82FYaYMcPQYIL+EUty/D9UYiLKVpHJ3uyXbr6lSwVBJQ==")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key","InstanceName")
		$oXMLAdd.SetAttribute("value","$instance")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "ChartImageHandler")
		$oXMLAdd.SetAttribute("value","storage=file;timeout=20;dir=c:\Resources\temp\;")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "piwikID")
		$oXMLAdd.SetAttribute("value","$siteId")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "DesignNumber")
		$oXMLAdd.SetAttribute("value","2")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "staffIp")
		$oXMLAdd.SetAttribute("value","")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "piwikApi")
		$oXMLAdd.SetAttribute("value","552b2fcbfa70464187ede096472ee5de")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "initUsers")
		$oXMLAdd.SetAttribute("value","False")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "bgColourHex")
		$oXMLAdd.SetAttribute("value","#000")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "navFontColourHex")
		$oXMLAdd.SetAttribute("value","#bf9e51")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "fontColourHex")
		$oXMLAdd.SetAttribute("value","#666")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "checkoutBgColourHex")
		$oXMLAdd.SetAttribute("value","#bf9e51")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "checkoutFontColourHex")
		$oXMLAdd.SetAttribute("value","#FFF")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "productBreakdown")
		$oXMLAdd.SetAttribute("value","false")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "invoiceLogoHeight")
		$oXMLAdd.SetAttribute("value","5cm")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "voucherLogoHeightWidth")
		$oXMLAdd.SetAttribute("value","40,0,140,140")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "snowEffect")
		$oXMLAdd.SetAttribute("value","False")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "xmasImage")
		$oXMLAdd.SetAttribute("value","False")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "customLogo")
		$oXMLAdd.SetAttribute("value","$instance-logo.png")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "sentence1")
		$oXMLAdd.SetAttribute("value","THIS WEBSITE IS FOR TESTING PURPOSES ONLY - NO ORDERS WILL BE HONORED")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "voucherMaker")
		$oXMLAdd.SetAttribute("value","False")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "onlineCommission")
		$oXMLAdd.SetAttribute("value","10")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "offlineCommission")
		$oXMLAdd.SetAttribute("value","5")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "mobileSubAccount")
		$oXMLAdd.SetAttribute("value","mobile")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "onlineSubAccount")
		$oXMLAdd.SetAttribute("value","internet")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "offlineSubAccount")
		$oXMLAdd.SetAttribute("value","internet")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "inProduction")
		$oXMLAdd.SetAttribute("value","false")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "cards")
		$oXMLAdd.SetAttribute("value","visa,mastercard,american-express,maestro")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "timezone")
		$oXMLAdd.SetAttribute("value","Europe/London")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "addOnsEnabled")
		$oXMLAdd.SetAttribute("value","True")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "promoCodes")
		$oXMLAdd.SetAttribute("value","True")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "gbPost")
		$oXMLAdd.SetAttribute("value","true")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "StripeEnabled")
		$oXMLAdd.SetAttribute("value","true")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "StripeApiPublicKey")
		$oXMLAdd.SetAttribute("value","pk_live_L3zjf8FFrjuslB5wfNjx2Ajh,pk_test_RU8kNsEjgHy3OPfNKPUWVd8i")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "StripeApiSecretKey")
		$oXMLAdd.SetAttribute("value","sk_live_dshwnfdI7PIiIbD2G7TchtPt,sk_test_OLbMlvyCxiJXbc2fLkFCYILp")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("key", "StripeConnect")
		$oXMLAdd.SetAttribute("value","ca_6BTD8c2ch0dgse6ccOWO8A1BCs8AvB87,ca_6BTDea8Okp9XF7PAxgJ6qqxQ6PBZuQ52")

		$oXMLDocument.Save("C:\Users\Administrator\Desktop\test\Web.appSettings.$instance.config")
		$configFiles0="appSetting"
	}

	if (Test-Path -Path "C:\Users\Administrator\Desktop\test\Web.connectionStrings.$instance.config")
	{
		Write-Output "C:\Users\Administrator\Desktop\test\Web.connectionStrings.$instance.config exists!"
	}
	else
	{
		# Create a new XML File with config root node
		[System.XML.XMLDocument]$oXMLDocument=New-Object System.XML.XMLDocument
		# New Node
		[System.XML.XMLElement]$oXMLRoot=$oXMLDocument.CreateElement("connectionStrings")
		# Append as child to an existing node
		$oXMLDocument.appendChild($oXMLRoot)

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("name", "DefaultConnection")
		$oXMLAdd.SetAttribute("connectionString", $connectionString + "Database=aspnet-EnjoviaManager-test;")
		$oXMLAdd.SetAttribute("providerName","System.Data.SqlClient")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("name", "VouchAppManager")
		$oXMLAdd.SetAttribute("connectionString", $connectionString + "Database=EnjoviaManager-test;")
		$oXMLAdd.SetAttribute("providerName","System.Data.SqlClient")

		[System.XML.XMLElement]$oXMLAdd=$oXMLRoot.appendChild($oXMLDocument.CreateElement("add"))
		$oXMLAdd.SetAttribute("name", "ProductContext")
		$oXMLAdd.SetAttribute("connectionString",$connectionString + "Database=VoucherShop-$instance;")
		$oXMLAdd.SetAttribute("providerName","System.Data.SqlClient")

		$oXMLDocument.Save("C:\Users\Administrator\Desktop\test\Web.connectionStrings.$instance.config")
		$configFiles1="connString"
	}

	$mailmessage = New-Object system.net.mail.mailmessage

	if (($configFiles0 -eq "appSetting") -and ($configFiles1 -ne "connString"))
	{
		$attachment = New-Object System.Net.Mail.Attachment("C:\Users\Administrator\Desktop\test\Web.appSettings.$instance.config", 'text/plain')
		$mailmessage.Attachments.Add($attachment)
	}
	elseif (($configFiles0 -ne "appSetting") -and ($configFiles1 -eq "connString"))
	{
		$attachment = New-Object System.Net.Mail.Attachment("C:\Users\Administrator\Desktop\test\Web.connectionStrings.$instance.config", 'text/plain')
		$mailmessage.Attachments.Add($attachment)
	}
	elseif (($configFiles0 -eq "appSetting") -and ($configFiles1 -eq "connString"))
	{
		$attachment1 = New-Object System.Net.Mail.Attachment("C:\Users\Administrator\Desktop\test\Web.appSettings.$instance.config", 'text/plain')
		$mailmessage.Attachments.Add($attachment1)
		$attachment2 = New-Object System.Net.Mail.Attachment("C:\Users\Administrator\Desktop\test\Web.connectionStrings.$instance.config", 'text/plain')
		$mailmessage.Attachments.Add($attachment2)
	}

	if ((($configFiles0 -eq "appSetting") -and ($configFiles1 -ne "connString")) -or (($configFiles0 -ne "appSetting") -and ($configFiles1 -eq "connString")) -or (($configFiles0 -eq "appSetting") -and ($configFiles1 -eq "connString")))
	{
		$mailmessage.from = ("noreply@enjovia.com")
		$mailmessage.To.add("risaac@enjovia.com")
		$mailmessage.Subject = "$instance config files"
		$mailmessage.Body = "Please find attached the new config files for $instance."

		#$mailmessage.IsBodyHTML = $true
		$SMTPClient = New-Object Net.Mail.SmtpClient("smtp.sendgrid.net", 587)
		$SMTPClient.Credentials = New-Object System.Net.NetworkCredential("7Yd5kqNYjG7LJDeViY6a", "7Yd5kqNYjG7Yd5kqNYjG")
		$SMTPClient.Send($mailmessage)
	}
