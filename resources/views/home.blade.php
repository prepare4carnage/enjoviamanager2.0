@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Pre-production <span"><a href="#">Add</a></span></div>

                <div class="panel-body">
                    <div class="container">
                        @foreach($preProductionList as $preProduction)
                            <div class="row">
                                {{ $preProduction }}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Production</div>

                <div class="panel-body">
                     <div class="container">
                        @foreach($productionList as $production)
                            <div class="row">
                                {{ $production }}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection