@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action=" {{ route('setNewInstance') }}">
                    {{ csrf_field() }}
                    <fieldset>

                    <!-- Form Name -->
                    <legend>New Instance</legend>

                    <!-- Text input-->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="textinput">Instance:</label>  
                      <div class="col-md-4">
                      <input id="instance" name="instance" type="text" class="form-control input-md">
                      <!-- <span class="help-block">help</span>   -->
                      </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="textinput">Domain:</label>  
                      <div class="col-md-4">
                      <input id="domain" name="domain" type="text" class="form-control input-md">
                      </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="textinput">Customer Name:</label>  
                      <div class="col-md-4">
                      <input id="customerName" name="customerName" type="text" class="form-control input-md">
                      <!-- <span class="help-block">help</span>   -->
                      </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="textinput">Telephone:</label>  
                      <div class="col-md-4">
                      <input id="telephone" name="telephone" type="tel" class="form-control input-md">
                      <!-- <span class="help-block">help</span>   -->
                      </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="textinput">Address:</label>  
                      <div class="col-md-4">
                      <input id="address" name="address" type="text" class="form-control input-md">
                      </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>

                    </fieldset>
                    </form>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>
@endsection